import Vuex from "vuex"

export default () => {
    return new Vuex.Store({
        strict: process.env.NODE_ENV !== 'production',
        state: {
            isLogin: false,
            userdata: false,
            product: [],
            banner: [],
        },
        mutations: {
            setLogin(state, data) {
                state.isLogin = data
            },
            setUserData(state, data) {
                state.userdata = data
            },
            onAuthStateChangedAction(state, data) {
                state.isLogin = (data.authUser != null) ? true : false
                state.userdata = (data.authUser != null) ? data.authUser.uid : false
            },

            //banner
            loadbanner(state, data) {
                state.banner = data
            },
            pushAnebanner(state, data) {
                state.banner.push(data)
            },
            editbanner(state, data) {
                const index = state.banner.findIndex(find => find.id === data.id)
                if (index !== -1) {
                    state.banner.splice(index, 1, data)
                }
            },
            removebanner(state, data) {
                const index = state.banner.findIndex(find => find.id === data)
                if (index !== -1) {
                    state.banner.splice(index, 1)
                }
            },

            //article
            loadarticle(state, data) {
                state.article = data
            },
            pushAnearticle(state, data) {
                state.article.push(data)
            },
            editarticle(state, data) {
                const index = state.article.findIndex(find => find.id === data.id)
                if (index !== -1) {
                    state.article.splice(index, 1, data)
                }
            },
            removearticle(state, data) {
                const index = state.article.findIndex(find => find.id === data)
                if (index !== -1) {
                    state.article.splice(index, 1)
                }
            },

            //product
            loadproduct(state, data) {
                state.product = data
            },
            pushAneproduct(state, data) {
                const index = state.product.findIndex(find => find.id === data.id)
                if (index !== -1) {
                    state.product.splice(index, 1, data)
                } else {
                    state.product.push(data)
                }
            },
            editproduct(state, data) {
                const index = state.product.findIndex(find => find.id === data.id)
                if (index !== -1) {
                    state.product.splice(index, 1, data)
                }
            },
            removeproduct(state, data) {
                const index = state.product.findIndex(find => find.id === data)
                if (index !== -1) {
                    state.product.splice(index, 1)
                }
            },

        },
        actions: {
            async nuxtServerInit(vuexContext) {
                // await vuexContext.dispatch('loadproduct')
                // await vuexContext.dispatch('loadarticle')
                // await vuexContext.dispatch('loadbanner')
            },

            //product
            async loadproduct(vuexContext) {
                const db = this.$fireDb.ref('product')
                const data = await db.once('value')
                let product = []
                data.forEach(function (childSnapshot) {
                    product.push({ ...childSnapshot.val(), id: childSnapshot.key });
                });
                vuexContext.commit("loadproduct", product)
            },
            pushAneproduct(vuexContext, data) {
                vuexContext.commit("pushAneproduct", data)
            },
            editproduct(vuexContext, data) {
                vuexContext.commit("editproduct", { ...data })
            },
            removeproduct(vuexContext, data) {
                vuexContext.commit("removeproduct", data)
            },
            removeproductall(vuexContext) {
                vuexContext.commit("removeproductall")
            },


            //article
            async loadarticle(vuexContext) {
                const db = this.$fireDb.ref('article')
                const data = await db.once('value')
                let article = []
                data.forEach(function (childSnapshot) {
                    article.push({ ...childSnapshot.val(), id: childSnapshot.key });
                });
                vuexContext.commit("loadarticle", article)
            },
            pushAnearticle(vuexContext, data) {
                vuexContext.commit("pushAnearticle", data)
            },
            editarticle(vuexContext, data) {
                vuexContext.commit("editarticle", { ...data })
            },
            removearticle(vuexContext, data) {
                vuexContext.commit("removearticle", data)
            },

            //banner
            async loadbanner(vuexContext) {
                const db = this.$fireDb.ref('banner')
                const data = await db.once('value')
                let banner = []
                data.forEach(function (childSnapshot) {
                    banner.push({ ...childSnapshot.val(), id: childSnapshot.key });
                });
                vuexContext.commit("loadbanner", banner)
            },
            pushAnebanner(vuexContext, data) {
                vuexContext.commit("pushAnebanner", data)
            },
            editbanner(vuexContext, data) {
                vuexContext.commit("editbanner", { ...data })
            },
            removebanner(vuexContext, data) {
                vuexContext.commit("removebanner", data)
            },

            //auth
            onAuthStateChangedAction(vuexContext, data) {
                vuexContext.commit("setLogin", (data.authUser != null) ? true : false)
                vuexContext.commit("setUserData", data.authUser.uid)
            },
            setLogin(vuexContext, data) {
                vuexContext.commit("setLogin", data)
            },
        },
        getters: {
            isLogin(state) {
                return state.isLogin
            },
            allstate(state) {
                return state
            }
        }
    });
}