import colors from 'vuetify/es5/util/colors'

export default {
  /*
  ** Nuxt rendering mode
  ** See https://nuxtjs.org/api/configuration-mode
  */
  mode: 'universal',
  /*
  ** Nuxt target
  ** See https://nuxtjs.org/api/configuration-target
  */
  target: 'server',
  /*
  ** Headers of the page
  ** See https://nuxtjs.org/api/configuration-head
  */
  head: {
    titleTemplate: '%s - ' + process.env.npm_package_name,
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Global CSS
  */
  css: [
  ],
  /*
  ** Plugins to load before mounting the App
  ** https://nuxtjs.org/guide/plugins
  */
  plugins: [
  ],
  /*
  ** Auto import components
  ** See https://nuxtjs.org/api/configuration-components
  */
  components: true,
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    '@nuxtjs/vuetify',
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/pwa',
    [
      '@nuxtjs/firebase',
      {
        config: {
          apiKey: "AIzaSyCo9CSQBhjWqSJoqeXgTIdRF15a5vhRykY",
          authDomain: "movie-d6546.firebaseapp.com",
          databaseURL: "https://movie-d6546.firebaseio.com",
          projectId: "movie-d6546",
          storageBucket: "movie-d6546.appspot.com",
          messagingSenderId: "321108032510",
          appId: "1:321108032510:web:afdc81736d156e665edeaf",
          measurementId: "G-3KY06PNTG9"
        },
        services: {
          auth: {
            ssr: true,
            initialize: {
              onAuthStateChangedMutation: 'onAuthStateChangedAction',
            }
          },
          realtimeDb: true,
          storage: true,
          analytics: true,
        }
      }
    ]
  ],

  pwa: {
    workbox: {
      runtimeCaching: [
        {
          urlPattern: '/',
          handler: 'cacheFirst'
        },
        {
          urlPattern: 'https://fonts.googleapis.com/.*',
          handler: 'cacheFirst'
        },
        {
          urlPattern: 'https://fonts.gstatic.com/.*',
          handler: 'cacheFirst'
        },
        {
          urlPattern: 'https://ik.imagekit.io/cii3/.*',
          handler: 'cacheFirst'
        },
        {
          urlPattern: 'https://www.googleapis.com/.*',
          handler: 'cacheFirst'
        },
        {
          urlPattern: 'https://securetoken.googleapis.com/.*',
          handler: 'cacheFirst'
        },
        {
          urlPattern: 'https://movie-d6546.firebaseio.com.*',
          handler: 'cacheFirst'
        },
      ],
    },
  },

  /*
  ** vuetify module configuration
  ** https://github.com/nuxt-community/vuetify-module
  */
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: true,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },
  /*
  ** Build configuration
  ** See https://nuxtjs.org/api/configuration-build/
  */
  build: {
  }
}
